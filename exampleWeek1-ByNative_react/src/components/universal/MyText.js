import React, { Component } from 'react'
import { Text, StyleSheet } from 'react-native'

const style = StyleSheet.create({
    text: {
        fontFamily: 'FC PorRak Regular',
        fontSize: 50,
    }
})

export default class MyText extends Component {
    state = {  }
    render() {
        return (
            <Text style={[style.text, this.props.style, { fontFamily: this.props.bold ? 'FC PorRak Bold' : 'FC PorRak Regular' }]}>
                {this.props.children}
            </Text>
        )
    }
}
