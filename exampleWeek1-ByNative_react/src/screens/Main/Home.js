import React, { Component } from 'react'
import { View, Text, Button, StatusBar, FlatList, ImageBackground } from 'react-native'
import SafeAreaView from 'react-native-safe-area-view'
import MyText from 'src/components/universal/MyText'

class Home extends Component {
    state = {
        randomNumbers: [ 2,6,8,8,5,4,3,76,8,9,4,3,76,89,9,9,6,344,48 ],
        isRefresh: false,
     }
    componentDidMount() {
        console.log('hello')
        // StatusBar.setTranslucent(true)
    }

    myRender = ({item}) => {
        return (
            <View style={{ borderWidth: 1, borderColor: '#000,', padding: 20, }}>
                <Text>{item}</Text>
            </View>
        )
    }

    workingRefresh = () => {
        this.setState({ isRefresh: true })
        setTimeout(() => {
            this.setState({ isRefresh: false })
            alert('eiei')
        }, 2000)
    }

    render() {
        return (
            <ImageBackground source={{ uri: 'https://i.ytimg.com/vi/X1vTNMQEnEk/maxresdefault.jpg' }} style={{flex: 1}}>
            <SafeAreaView style={{ flex:1, }} forceInset={{ top: 'always' }}>
                <StatusBar translucent={true} backgroundColor="transparent" />
                <FlatList
                    data={this.state.randomNumbers}
                    renderItem={this.myRender}
                    style={{ height: 500 }}
                    keyExtractor={(item, index) => index + ''}
                    refreshing={this.state.isRefresh}
                    onRefresh={this.workingRefresh}
                />
                <Button
                    title="Go to Detail"
                    onPress={() => this.props.navigation.navigate('Detail', {
                        name: 'ทรัพย์'
                    })}
                />
            </SafeAreaView>
        </ImageBackground>
        )
    }
}

export default Home