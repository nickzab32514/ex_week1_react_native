import React, { Component } from 'react'
import NameContext from 'src/context/Name'

export default function (Component) {
    return class WithName extends Component {
        static contextTypes = NameContext
        render() {
            return <Component name={this.context} />
        }
    }
}