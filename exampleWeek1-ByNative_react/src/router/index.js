import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Main from './Main'
import Profile from './Profile'

const Tab = createBottomTabNavigator()

export default class Router extends Component {
    state = {  }
    render() {
        return (
            <NavigationContainer>
                {/* <Main /> */}
                <Profile />
                {/* <Tab.Navigator>
                    <Tab.Screen name="Main" component={Main} />
                    <Tab.Screen name="Profile" component={Profile} />
                </Tab.Navigator> */}
            </NavigationContainer>
        )
    }
}