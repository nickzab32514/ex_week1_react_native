import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Home from 'src/screens/Main/Home'
import Detail from 'src/screens/Main/Detail'
const Stack = createStackNavigator()
export default class Main extends Component {
    state = {  }
    render() {
        return (
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="Detail" component={Detail} />
            </Stack.Navigator>
        )
    }
}