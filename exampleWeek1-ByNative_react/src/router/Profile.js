import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import Me from 'src/screens/Profile/Me'
import Setting from 'src/screens/Profile/Setting'

const Stack = createStackNavigator()
export default class Profile extends Component {
    state = {  }
    render() {
        return (
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="Me" component={Me} />
                <Stack.Screen name="Setting" component={Setting} />
            </Stack.Navigator>
        )
    }
}