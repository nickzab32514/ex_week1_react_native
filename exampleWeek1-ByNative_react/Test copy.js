import React, { Component } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'

export default class Test extends Component {
    state = {  }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ alignItems: 'center', justifyContent: 'center', height: 80, }}><Text>News</Text></View>
                <View style={{flex: 1, marginHorizontal: 8, }}>
                    <View style={{ backgroundColor: 'green', justifyContent: 'center', alignItems: 'center', height: 200, marginBottom: 4, }}>
                        <Text>Lorem</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row'}}>
                        <View style={{ flex: 1, }}>
                            <View style={{ flex: 1, backgroundColor: 'green', marginBottom: 4, justifyContent: 'center', alignItems: 'center' }} ><Text>Lorem</Text></View>
                            <View style={{ flex: 3, backgroundColor: 'green', marginBottom: 4, justifyContent: 'center', alignItems: 'center' }} ><Text>Lorem</Text></View>
                            <View style={{ flex: 2, backgroundColor: 'green', marginBottom: 4, justifyContent: 'center', alignItems: 'center' }} ><Text>Lorem</Text></View>
                        </View>
                        <View style={{ flex:1 , marginLeft: 4,}}>
                            <View style={{ flex: 2, backgroundColor: 'green', marginBottom: 4, justifyContent: 'center', alignItems: 'center' }} ><Text>Lorem</Text></View>
                            <View style={{ flex: 1, backgroundColor: 'green', marginBottom: 4, justifyContent: 'center', alignItems: 'center' }} ><Text>Lorem</Text></View>
                            <View style={{ flex: 3, backgroundColor: 'green', marginBottom: 4, justifyContent: 'center', alignItems: 'center' }} ><Text>Lorem</Text></View>
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: 96,}}>
                    <Button title="Read More" />
                </View>
            </View>
        )
    }
}