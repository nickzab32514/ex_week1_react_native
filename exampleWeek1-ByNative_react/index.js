/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
import {name as appName} from './app.json';
// import Test from './Test'
import App from './src/App'

import 'react-native-gesture-handler'

AppRegistry.registerComponent(appName, () => App);
