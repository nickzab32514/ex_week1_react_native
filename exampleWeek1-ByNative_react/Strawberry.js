import React, { Component } from 'react'
import { View, Text, Button } from 'react-native'
export default class Strawberry extends Component {
    state = {  }
    render() {
        return (
            <View>
                <Text>Strawberry</Text>
                <Button
                    title="Go to Strawberer"
                    onPress={() => this.props.navigation.navigate('Strawberer')}
                />
            </View>
        )
    }
}